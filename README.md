# Rehearsal Basement Server

### Hello!
Glad to see you. This repository is intended for server of [Rehearsal Basement app](https://bitbucket.org/AntipovAlex/rehearsalbasement)

### What is Rehaearsal Basement?
In a word, it's an android application for online booking, follow the [link](https://bitbucket.org/AntipovAlex/rehearsalbasement), to learn more.

### How it works?
It's a REST-based web server, which works on *Tomcat*. 
To develop that, I used next things:

 - *Java*
 - *Spring Boot*
 - *Spring Security*
 - *JWT* (JJWT/JWTK)](https://github.com/jwtk/jjwt)
 - *MySQL*
 - *ORM* (Hibernate)
 - *GSON*
 - *Quartz* [(scheduler)](http://www.quartz-scheduler.org/)
 - *Maven*

# API

There is the requests description and examples of answers.

**Registration**

POST
```
http://host.name/user/register
```

| Parameter | Type | Description |
| --------- | ---- | ----------- |
| username  | String | Login |
| password  | String | Password |
| first | String | First name |
| last | String | Second name |
| email | String | Email |
| phone | String | Tel. |

Success:
```
{
  "status": true,
  "error": "",
  "data": null
}
```

Failure:
```
{
  "status": false,
  "error": "Пользователь с таким именем уже существует",
  "data": null
}
```

**Authentication**

POST
```
http://host.name/user/auth
```

| Parameter | Type | Description |
| --------- | ---- | ----------- |
| Username  | String | Login |
| Password  | String | Password |
| Device Token | String | Firebase Device Token |

Success:
```
{
  "status": true,
  "error": "",
  "data": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImNyZWF0ZWQiOjE0OTU1NTk5MTA4Njh9.tLxaktN
T87mQYvbXTTfW57qBYkQm6sJoVbGO8_pE-g6PLyeVAUpp-_BnlQa6nk-m0ye9iz3IggbRWyDFBbF9Gg"
}
```

Failure:
```
{
  "status": false,
  "error": "Bad credentials",
  "data": null
}
```
**Get user info**

GET

*Authenticated user only*

```
http://host.name/user/me
```

| Parameter | Type | Description |
| --------- | ---- | ----------- |
| User Token | String | User token. Put it to "Authentication" header

Success:
```
{
  "status": true,
  "error": "",
  "data": {
    "username": "admin",
    "firstName": "Alexander",
    "lastName": "Antipov",
    "email": "alexander@antipov.com",
    "phone": "380932686097",
    "deviceToken": null
  }
}
```

Failure:
```
{
  "status": false,
  "error": "Неверные авторизационные данные",
  "data": null
}
```

**Add booking**

POST

```
http://host.name/booking/set
```

| Parameter | Type | Description |
| --------- | ---- | ----------- |
| year | Integer | Year |
| month | Integer | Month |
| day | Integer | Day |
| times | String | Times, which will be booked (comma separated) |
| name | String | Name |
| telephone | String | Tel. |
| comment | String | Comment (optional) |
| type | Enumeration {0, 1, 2} | Type of the booking |
Success:
```
{
  "status": true,
  "error": "",
  "data": null
}
```

Failure:
```
{
  "status": false,
  "error": "Ошибка при создании записи: Это время уже занято",
  "data": null
}
```

**Get booking**

GET

Get bookings for a day

```
http://host.name/booking/get
```

| Parameter | Type | Description |
| --------- | ---- | ----------- |
| year | Integer | Year |
| month | Integer | Month |
| day | Integer | Day |

Success:
```
{
  "status": true,
  "error": "",
  "data": [
    {
      "id": 130,
      "name": "alex",
      "telephone": "380932686097",
      "comment": "qqweqw",
      "type": 0,
      "time": 1525600800000,
      "admin_comment": null
    },
    .....
    {
      "id": 134,
      "name": "alex",
      "telephone": "380932686097",
      "comment": "qqweqw",
      "type": 0,
      "time": 1525615200000,
      "admin_comment": null
    }
  ]
}
```

Failure:
```
{
  "status": false,
  "error": "",
  "data": null
}
```

**Get booking by key**

That method returns paginated bookings by one of criteria: past, today, in future

GET

Get bookings for a day

```
http://host.name/booking/get/by
```

| Parameter | Type | Description |
| --------- | ---- | ----------- |
| key | Enumeration {past, today, infuture} | key |
| page | Integer | page |

Success:
```
{
  "status": true,
  "error": "",
  "data": [
    {
      "id": 130,
      "name": "alex",
      "telephone": "380932686097",
      "comment": "qqweqw",
      "type": 0,
      "time": 1525600800000,
      "admin_comment": null
    },
    .....
    {
      "id": 134,
      "name": "alex",
      "telephone": "380932686097",
      "comment": "qqweqw",
      "type": 0,
      "time": 1525615200000,
      "admin_comment": null
    }
  ]
}
```

Failure:
```
{
  "status": false,
  "error": "Ошибка получения данных",
  "data": null
}
```

**Get booking notification**

*Authenticated user only*

GET

That method returns count of today bookings

```
http://host.name/booking/get/notification
```

Success:
```
{
  "status": true,
  "error": "",
  "data": "4"
}
```

Failure:
```
{
  "status": false,
  "error": "Ошибка получения данных",
  "data": null
}
```

Thank you for reading!
Please, note: I'm working on it now, it's alfa-version, not even beta. Bye!