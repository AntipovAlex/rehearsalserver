package com.rehearsalserver.API;

import com.rehearsalserver.model.entity.FirebaseSendMessageResponse;
import com.rehearsalserver.model.entity.NotificationRequestEntity;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Antipov on 02.06.17.
 */
public interface FirebaseMessaging {
    @POST("/fcm/send")
    Call<FirebaseSendMessageResponse> sendMessage(@Header("Content-Type") String contentType,
                                                  @Header("Authorization") String key,
                                                  @Body NotificationRequestEntity body);
}
