package com.rehearsalserver;

import com.rehearsalserver.jobs.NotifyAboutTodayBookingsJob;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

import static com.rehearsalserver.util.Constants.CRONEXPRESSION;

/**
 * Created by antipov on 11.04.17.
 */

@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }

    public static void main(String[] args) {
        // running app
        SpringApplication.run(Application.class, args);
        NotifyAboutTodayBookingsJob.startJob(CRONEXPRESSION);
    }
}
