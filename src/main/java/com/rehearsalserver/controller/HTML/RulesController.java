package com.rehearsalserver.controller.HTML;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

@RestController
public class RulesController {
    @Value("${rules.path}")
    private String rulesPath;

    @RequestMapping(value = "/rules", method = RequestMethod.GET)
    public String getRules() {
        StringBuilder result = new StringBuilder();
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(rulesPath);
        System.out.println("Var path "+rulesPath);
        System.out.println("File path "+file.getAbsolutePath());
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                result.append(line).append("\n");
            }
            scanner.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result.toString();
    }
}
