package com.rehearsalserver.controller.Error;

import com.rehearsalserver.model.response.Response;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.List;

/**
 * Created by Antipov on 23.04.17.
 */
@RestController
@ControllerAdvice
public class ErrorHandlerController {
    @ExceptionHandler(AuthenticationException.class)
    public Response notAuthErrorHandler(AuthenticationException e){
        return Response.fail(null, "Неверные авторизационные данные");
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public Response validationErrorHandler(ConstraintViolationException e){
        StringBuilder s = new StringBuilder();
        for (ConstraintViolation error : e.getConstraintViolations()){
            s.append(", ").append(error.getMessage());
        }
        return Response.fail(null, s.toString());
    }
}
