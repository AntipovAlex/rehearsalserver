package com.rehearsalserver.controller.Booking;

import com.rehearsalserver.model.response.Response;
import com.rehearsalserver.util.GoogleMailer;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.*;
import java.util.HashMap;
import java.util.Map;

import static com.rehearsalserver.util.Constants.EMAIL_PASSWORD;
import static com.rehearsalserver.util.Constants.EMAIL_USERNAME;
import static com.rehearsalserver.util.Constants.REG_EXP_PHONE;

@RestController
@Validated
public class SetBookingNotification {

    @RequestMapping(value = "/booking/set/notification", method = RequestMethod.POST)
    public Response setBookingNotification(
            @NotNull
            @NotEmpty
            @NotBlank
            @Size(min = 2, max = 15, message = "Имя должно быть от 2 до 15 символов") String name,

            @NotNull(message = "Некорректный год")
            @Min(value = 2017, message = "Некорректный год") Integer year,

            @NotNull(message = "Некорректный месяц")
            @Min(value = 1, message = "Некорректный месяц")
            @Max(value = 12, message = "Некорректный месяц") Integer month,

            @NotNull
            @Min(value = 1, message = "Некорректный день")
            @Max(value = 31, message = "Некорректный день") Integer day,

            @NotNull
            @NotEmpty
            @NotBlank
                    //todo: there mustbe a regex
                    String time,

            @NotNull
            @NotEmpty
            @NotBlank
            @Pattern(regexp = REG_EXP_PHONE) String telephone
    ){
        try{
            //set crednrials for mailing
            Map<String, String> credentials = new HashMap<String, String>();
            credentials.put("username",  EMAIL_USERNAME);
            credentials.put("password", EMAIL_PASSWORD);
            String messageSubject = "Сообщите, если освободится: "+day+"."+month+"."+year;
            String messageText = name + " просит сообщить, если освободится: "+day+"."+month+"."+year+
                    ", время: "+time+"\n\n"+"Контакты: "+telephone;

            // sending email notification to admin
            // made it in parallel thread to avoid user long wait
            GoogleMailer mail = new GoogleMailer(credentials, messageSubject, messageText);
            mail.start();
            return Response.ok(null);
        } catch (Throwable t){
            t.printStackTrace();
            return Response.fail(null, "Ошибка при создании заявки: ".concat(t.getMessage()));
        }
    }
}
