package com.rehearsalserver.controller.Booking;

import com.rehearsalserver.service.BookingService;
import com.rehearsalserver.util.HibernateUtil;
import com.rehearsalserver.model.entity.BookingEntity;
import com.rehearsalserver.model.entity.UserEntity;
import com.rehearsalserver.model.response.Response;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.rehearsalserver.util.Constants.PAGESIZE;

/**
 * Created by Antipov on 22.04.17.
 */
@RestController
@Validated
public class GetBookingByKeyController {
    @Resource(name = "bookingService")
    BookingService bookingService;

    @RequestMapping("/booking/get/by")
    public Response getBy(
            @NotNull (message = "Параметр обязателен")
            @NotBlank (message = "Значение не должно быть пробелом")
            @NotEmpty (message = "Значение не должно быть пустым") String key,

            @NotNull (message = "Параметр обязателен")
            @Min(value = 1, message = "Минимально - 1") Integer page){
        try {
            return Response.ok(bookingService.getByKey(key, page));

        }catch (Throwable t){
            t.printStackTrace();
            return Response.fail(null, "Ошибка получения данных");
        }
    }

}
