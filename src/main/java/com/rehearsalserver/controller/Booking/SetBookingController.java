package com.rehearsalserver.controller.Booking;

import com.rehearsalserver.model.entity.BookingEntity;
import com.rehearsalserver.model.entity.UserEntity;
import com.rehearsalserver.model.response.Response;
import com.rehearsalserver.service.BookingService;
import com.rehearsalserver.util.GoogleMailer;
import com.rehearsalserver.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.rehearsalserver.util.Constants.EMAIL_PASSWORD;
import static com.rehearsalserver.util.Constants.EMAIL_USERNAME;
import static com.rehearsalserver.util.Constants.REG_EXP_PHONE;

/**
 * Created by antipov on 11.04.17.
 */
@RestController
@Validated
public class SetBookingController {
    @Resource(name = "bookingService")
    BookingService bookingService;

    @RequestMapping(name = "/booking/set", method = RequestMethod.POST)
    public Response setBookings(
            @NotNull(message = "Некорректный год")
            @Min(value = 2017, message = "Некорректный год") Integer year,

            @NotNull(message = "Некорректный месяц")
            @Min(value = 1, message = "Некорректный месяц")
            @Max(value = 12, message = "Некорректный месяц") Integer month,

            @NotNull
            @Min(value = 1, message = "Некорректный день")
            @Max(value = 31, message = "Некорректный день") Integer day,

            @NotNull
            @NotEmpty
            @NotBlank
                    //todo: there mustbe a regex
                    String times,
            @NotNull
            @NotEmpty
            @NotBlank
            @Size(min = 2, max = 25, message = "Имя должно быть от 2 до 15 символов") String name,

            @NotNull
            @NotEmpty
            @NotBlank
            @Pattern(regexp = REG_EXP_PHONE) String telephone,

            @Size(max = 200, message = "Комментарий не более 200 символов") String comment,

            @NotNull
            @Max(value = 2, message = "Указан неверный тип") Integer type
    ){
        try {
            bookingService.setBookingByDate(year, month, day, times, name, telephone, comment, type);
            List<Integer> time = new ArrayList<Integer>();
            for (String s : times.split(",")) {
                time.add(Integer.valueOf(s));
            }

            //set crednrials for mailing
            Map<String, String> credentials = new HashMap<String, String>();
            credentials.put("username",  EMAIL_USERNAME);
            credentials.put("password", EMAIL_PASSWORD);
            String messageSubject = type+": Новая бронь на "+day+"."+month+"."+year;
            String messageText = "Новая бронь на "+day+"."+month+"."+year+"\n";
            messageText += "\n\nВремя: "+time;
            messageText += "\n\nЗаказал: "+name;
            messageText += "\n\nНомер телефона: "+telephone;
            messageText += "\n\nКомментарий: "+comment;
            // sending email notification to admin
            // made it in parallel thread to avoid user long wait
            GoogleMailer mail = new GoogleMailer(credentials, messageSubject, messageText);
            mail.start();
            return Response.ok(null);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return Response.fail(null, "Ошибка при создании записи: ".concat(throwable.getMessage()));
        }
    }
}
