package com.rehearsalserver.controller.Booking;

import com.rehearsalserver.service.BookingService;
import com.rehearsalserver.model.response.Response;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.ParseException;

/**
 * Created by Antipov on 22.04.17.
 */
@RestController
public class GetBookingController {
    @Resource(name = "bookingService")
    BookingService bookingService;

    @RequestMapping("/booking/get")
    public Response getBookings(
            @RequestParam(name = "year", defaultValue = "0000") String year,
            @RequestParam(name = "month", defaultValue = "00") String month,
            @RequestParam(name = "day", defaultValue = "00") String day
    ){
        try {
//            int i = 1/0;
            return Response.ok(bookingService.getBookingByDay(year, month, day));
        } catch (Throwable t) {
            return Response.fail(null, t.getMessage());
        }
    }
}
