package com.rehearsalserver.controller.Booking;

import com.rehearsalserver.model.entity.BookingEntity;
import com.rehearsalserver.model.entity.UserEntity;
import com.rehearsalserver.model.response.Response;
import com.rehearsalserver.service.BookingService;
import com.rehearsalserver.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Calendar;
import java.util.Date;

import static com.rehearsalserver.util.Constants.PAGESIZE;

/**
 * Created by Antipov on 06.05.17.
 */
@RestController
public class GetBookingNotification {
    @Resource(name = "bookingService")
    BookingService bookingService;

    @RequestMapping("/booking/get/notification")
    public Response getNotification(){
        try{
            return Response.ok(bookingService.getNotification());
        }catch (Throwable t){
            t.printStackTrace();
            return Response.fail(null, "Ошибка получения данных");
        }

    }
}
