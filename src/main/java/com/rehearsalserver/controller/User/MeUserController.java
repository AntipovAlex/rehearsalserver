package com.rehearsalserver.controller.User;

import com.rehearsalserver.model.entity.UserEntity;
import com.rehearsalserver.model.response.Response;
import com.rehearsalserver.service.BookingService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created by Antipov on 22.04.17.
 */
@RestController
public class MeUserController {
    @Resource(name = "bookingService")
    BookingService bookingService;

    @RequestMapping("/user/me")
    public Response getUserDetails(@AuthenticationPrincipal UserEntity principal){
        // cause UserDetail implementation dont want return my custom user fields
        return Response.ok(new UserEntity(
                principal.getUsername(),
                principal.isEnabled(),
                principal.getFirstName(),
                principal.getLastName(),
                principal.getEmail(),
                principal.getPhone(),
                principal.getPasswordResetAt(),
                principal.getDeviceToken(),
                bookingService.getNotification()
        ));
    }
}
