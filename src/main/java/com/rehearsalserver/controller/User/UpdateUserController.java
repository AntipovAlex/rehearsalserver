package com.rehearsalserver.controller.User;

import com.rehearsalserver.model.entity.UserEntity;
import com.rehearsalserver.model.response.Response;
import com.rehearsalserver.service.UserService;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.rehearsalserver.util.Constants.REG_EXP_EMAIL;
import static com.rehearsalserver.util.Constants.REG_EXP_PHONE;

@RestController
@Validated
public class UpdateUserController {
    @Resource(name = "userService")
    private UserService userDetailsService;

    @RequestMapping(value = "/user/update-user", method = RequestMethod.POST)
    public Response updateUser(@AuthenticationPrincipal UserEntity principal, @NotNull
                    @NotEmpty
                    @NotBlank
                    @Size(min = 2, max = 15, message = "Имя должно быть от 2 до 15 символов") String first,

                   @NotNull
                   @NotEmpty
                   @NotBlank
                   @Size(min = 2, max = 15, message = "Фамилия должна быть от 2 до 15 символов") String last,

                   @NotNull
                   @NotEmpty
                   @NotBlank
                   @Pattern(regexp = REG_EXP_EMAIL, message = "Email должен быть в формате john@doe.com") String email,

                   @NotNull
                   @NotEmpty
                   @NotBlank
                   @Pattern(regexp = REG_EXP_PHONE) String phone){
        try {
            userDetailsService.updateUser(principal, first, last, email, phone);
            return Response.ok(null);
        } catch (Throwable e) {
            e.printStackTrace();
            return Response.fail(null, e.getMessage());
        }

    }
}
