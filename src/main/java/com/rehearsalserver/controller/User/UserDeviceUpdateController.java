package com.rehearsalserver.controller.User;

import com.rehearsalserver.model.entity.UserEntity;
import com.rehearsalserver.model.response.Response;
import com.rehearsalserver.service.UserService;
import com.rehearsalserver.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created by Antipov on 10.05.17.
 */

@RestController
public class UserDeviceUpdateController {
    @Resource(name = "userService")
    private UserService userDetailsService;

    @RequestMapping(value = "/user/update-device", method = RequestMethod.POST)
    public Response updateDeviceToken(@AuthenticationPrincipal UserEntity principal, String token){
        try {
            userDetailsService.updateDeviceToken(principal, token);
            return Response.ok(null);
        } catch (Throwable e) {
            e.printStackTrace();
            return Response.fail(null, e.getMessage());
        }

    }
}
