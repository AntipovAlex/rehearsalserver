package com.rehearsalserver.controller.User;

import com.rehearsalserver.model.entity.UserEntity;
import com.rehearsalserver.model.entity.UserRoleEntity;
import com.rehearsalserver.model.response.Response;
import com.rehearsalserver.service.UserService;
import com.rehearsalserver.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.sql.Timestamp;

import static com.rehearsalserver.util.Constants.*;

/**
 * Created by Antipov on 22.04.17.
 */

@RestController
@Validated
public class RegisterUserController {

    @Resource(name = "userService")
    private UserService userDetailsService;

    @RequestMapping(value ="/user/register", method = RequestMethod.POST)
    public Response registerUser(
            //TODO: доделать валидатор
            @NotNull(message = LOGINERROR)
            @NotEmpty(message = LOGINERROR)
            @NotBlank(message = LOGINERROR)
            @Size(min = 3, max = 15, message =  LOGINERRORSIZE) String username,

            @NotNull(message = PASSWORDERROR)
            @NotEmpty(message = PASSWORDERROR)
            @NotBlank(message = PASSWORDERROR)
            @Size(min = 6, max = 15, message = PASSWORDERRORSIZE) String password,

            @NotNull
            @NotEmpty
            @NotBlank
            @Size(min = 2, max = 15, message = "Имя должно быть от 2 до 15 символов") String first,

            @NotNull
            @NotEmpty
            @NotBlank
            @Size(min = 2, max = 15, message = "Фамилия должна быть от 2 до 15 символов") String last,

            @NotNull
            @NotEmpty
            @NotBlank
            @Pattern(regexp = REG_EXP_EMAIL, message = "Email должен быть в формате john@doe.com") String email,

            @NotNull
            @NotEmpty
            @NotBlank
            @Pattern(regexp = REG_EXP_PHONE) String phone
    ){
        try{
            userDetailsService.register(username, password, first, last, email, phone);
            return Response.ok(null);
        } catch (Throwable e) {
            e.printStackTrace();
            return Response.fail(null, e.getMessage());
        }
    }

}
