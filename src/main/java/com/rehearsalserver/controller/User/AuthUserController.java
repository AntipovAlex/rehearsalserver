package com.rehearsalserver.controller.User;

import com.rehearsalserver.model.entity.UserEntity;
import com.rehearsalserver.model.response.Response;
import com.rehearsalserver.security.utils.JWTTokenUtil;
import com.rehearsalserver.service.UserService;
import com.rehearsalserver.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.annotations.Parameter;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Created by Antipov on 22.04.17.
 */
@RestController
public class AuthUserController {

    @Resource(name = "userService")
    private UserService userDetailsService;

    @RequestMapping(value = "/user/auth", method = RequestMethod.POST)
    public Response getToken(@RequestParam(name = "username") String username,
                             @RequestParam(name = "password") String password,
                             @RequestParam(name = "device_token", required=false) String deviceToken) {
        try{
            return Response.ok(userDetailsService.getToken(username, password, deviceToken));
        } catch (Throwable e) {
            e.printStackTrace();
            return Response.fail(null, e.getMessage());
        }
    }

}
