package com.rehearsalserver.security.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rehearsalserver.model.response.Response;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

    private static final long serialVersionUID = -8970718410437077606L;

    @Override
    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException authException) throws IOException , AccessDeniedException {
        response.setContentType("application/json, charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        Gson g = new GsonBuilder().serializeNulls().create();
        response.getWriter().println(g.toJson(
                Response.fail(null,"Неверные авторизационные данные"), Response.class));

    }

}