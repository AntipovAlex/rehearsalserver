package com.rehearsalserver.jobs;

import com.rehearsalserver.API.FirebaseMessaging;
import com.rehearsalserver.model.entity.BookingEntity;
import com.rehearsalserver.model.entity.FirebaseSendMessageResponse;
import com.rehearsalserver.model.entity.NotificationRequestEntity;
import com.rehearsalserver.model.entity.UserEntity;
import com.rehearsalserver.service.BookingService;
import com.rehearsalserver.service.UserService;
import com.rehearsalserver.util.HibernateUtil;
import com.rehearsalserver.util.RetrofitUtil;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import retrofit2.Response;

import javax.annotation.Resource;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;

import static com.rehearsalserver.util.Constants.*;

/**
 * Created by Antipov on 17.05.17.
 */
public class NotifyAboutTodayBookingsJob implements Job {
//    @Resource(name = "bookingService")
//    BookingService bookingService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        List<BookingEntity> today = new BookingService().getBookingsForNotifications();
        sendNotification(today);
    }


    private void sendNotification(List<BookingEntity> today){
        HashSet<UserEntity> users = new HashSet<>(); // list of unique users
        for (BookingEntity booking:today){
            users.add(booking.getUsers_id());
        }
        FirebaseMessaging messaging = RetrofitUtil.sendBuilder().create(FirebaseMessaging.class);
        // sending notifications to users
        for (UserEntity user:users){
            try {
                messaging.sendMessage(SENDMESSAGECONTENTTYPE, FCMAPIKEY,
                        new NotificationRequestEntity(
                                user.getDeviceToken(),
                                "У вас на сегодня забронивано время.",
                                "Нажмите для просмотра.")).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void startJob(String cron) {
        // running job
        JobDetail job = new JobDetail();
        job.setGroup("NotificateUserJob");
        job.setName("NotificateUserJob");
        job.setJobClass(NotifyAboutTodayBookingsJob.class);

        //configure the scheduler time
        CronTrigger trigger = new CronTrigger();
        trigger.setName("NotificateUserTrigger");
        try {
            trigger.setCronExpression(cron);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //schedule it
        Scheduler scheduler;
        try {
            scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.start();
            scheduler.scheduleJob(job, trigger);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }

    }
}
