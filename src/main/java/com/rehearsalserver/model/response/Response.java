package com.rehearsalserver.model.response;

import java.util.ArrayList;

/**
 * Created by antipov on 11.04.17.
 */

public class Response {
    Boolean status;

    String error;

    Object data;

    public Response(Boolean status, String error, ArrayList<Object> data) {
        this.status = status;
        this.error = error;
        this.data = data;
    }

    public Response(Boolean status, String error, Object data) {
        this.status = status;
        this.error = error;
        this.data = data;
    }

    public static Response ok(ArrayList<Object> data){
        return new
                Response(true, "", data);
    }

    public static Response ok(Object data){
        return new
                Response(true, "", data);
    }
    public static Response fail(ArrayList<Object> data, String error){
        return new
                Response(false, error, data);
    }



    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
