package com.rehearsalserver.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Antipov on 18.05.17.
 */
public class NotificationRequestEntity implements Serializable {
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("to")
    @Expose
    private String to;

    public NotificationRequestEntity(String to, String title, String content) {
        this.data = new Data(title, content);
        this.to = to;
    }

    public Data getData() {
        return data;
    }

    public void setData(String title, String content) {
        this.data = new Data(title, content);
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

     class Data  implements Serializable{

        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("content")
        @Expose
        private String content;

         public Data(String title, String content) {
             this.title = title;
             this.content = content;
         }

         public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

    }

}
