package com.rehearsalserver.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OptimisticLockType;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Joseph K on 12.12.2016.
 */

/**
 * ENTITY - describes one booking record
 */
@Entity
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.ALL, dynamicUpdate = true)
@Table(name = "booking", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id")
})
public class BookingEntity {

    //entity fields
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "name", nullable = false, length = 30)
    private String name;

    @Column(name = "telephone", nullable = false, length = 15)
    private String telephone;

    @Column(name = "comment", length = 100)
    private String comment;

    @Column(name = "type", nullable = false)
    private Integer type;

    @Column(name = "time", nullable = false)
    private Timestamp time;


    @OneToOne
    @JoinColumn(name = "user_id")
    private UserEntity user;

    @Column(name = "admin_comment")
    private String admin_comment;

    @Column(name = "booked")
    private boolean booked;

    //default constructor
    public BookingEntity() {
    }

    public Integer  getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
    //setters/getters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @JsonIgnore
    public UserEntity getUsers_id() {
        return user;
    }

    public void setUsers_id(UserEntity user) {
        this.user = user;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public String getAdmin_comment() {
        return admin_comment;
    }

    public void setAdmin_comment(String admin_comment) {
        this.admin_comment = admin_comment;
    }

    public boolean isBooked() {
        return booked;
    }

    public void setBooked(boolean booked) {
        this.booked = booked;
    }
}

