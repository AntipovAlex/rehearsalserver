package com.rehearsalserver.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OptimisticLockType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by antipov on 24.02.17.
 */

@Entity
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.ALL, dynamicUpdate = true)
@Table(name = "users", uniqueConstraints = {
        @UniqueConstraint(columnNames = "id"),
        @UniqueConstraint(columnNames = "username"),
        @UniqueConstraint(columnNames = "email")
})
public class UserEntity implements UserDetails  {
    //entity fields
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @NotNull
    @Column(name = "username", nullable = false, length = 45)
    private String username;

    @NotNull
    @Column(name = "password", nullable = false, length = 60)
    private String password;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @NotNull
    @Column(name = "first_name", nullable = false, length = 45)
    String firstName;

    @NotNull
    @Column(name = "last_name", nullable = false, length = 45)
    String lastName;

    @NotNull
    @Column(name = "email", nullable = false, length = 45)
    String email;

    @NotNull
    @Column(name = "phone", nullable = false, length = 45)
    String phone;

    @Column(name = "password_reset_at", nullable = false)
    private Timestamp passwordResetAt;

    @Column(name = "device_token", length = 60)
    private String deviceToken;

    @OneToMany(mappedBy="user")
    private Set<UserRoleEntity> userRoles = new HashSet<UserRoleEntity>(0);

    public UserEntity() {}

    public UserEntity(String username, boolean enabled, String firstName, String lastName, String email, String phone,
                      Timestamp passwordResetAt, String deviceToken, int notifications) {
        this.username = username;
        this.enabled = enabled;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.passwordResetAt = passwordResetAt;
        this.deviceToken = deviceToken;
        this.notifications = notifications;
    }

    private int notifications;

    private String token;

    @JsonIgnore
    public Long getId() {
        return id;
    }

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    @JsonIgnore
    public Timestamp getPasswordResetAt() {
        return passwordResetAt;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPasswordResetAt(Timestamp passwordResetAt) {
        this.passwordResetAt = passwordResetAt;
    }

    public void setUserRoles(Set<UserRoleEntity> userRoles) {
        this.userRoles = userRoles;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @JsonIgnore
    public Set<UserRoleEntity> getUserRoles() {
        return userRoles;
    }


    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String device_token) {
        this.deviceToken = device_token;
    }

    public int getNotifications() {
        return notifications;
    }

    public void setNotifications(int notifications) {
        this.notifications = notifications;
    }
}
