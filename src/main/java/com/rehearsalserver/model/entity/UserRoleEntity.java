package com.rehearsalserver.model.entity;

import org.hibernate.annotations.OptimisticLockType;

import javax.persistence.*;

/**
 * Created by antipov on 24.02.17.
 */

@Entity
@org.hibernate.annotations.Entity(optimisticLock = OptimisticLockType.ALL, dynamicUpdate = true)
@Table(name = "user_roles", uniqueConstraints = {
        @UniqueConstraint(columnNames = "user_roles_id")
})
public class UserRoleEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_roles_id", unique = true, nullable = false)
    private Long id;

    @Column(name = "role", nullable = false)
    private String role;

    @ManyToOne
    @JoinColumn(name="user_id", referencedColumnName = "id")
    private UserEntity user;


    public UserRoleEntity(String role, UserEntity user) {
        this.role = role;
        this.user = user;
    }

    public UserRoleEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }
}
