package com.rehearsalserver.util;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.rehearsalserver.util.Constants.URLPOSTNOTIFICATION;

/**
 * Created by Antipov on 02.06.17.
 */
public class RetrofitUtil {
    public static Retrofit sendBuilder(){
        return new Retrofit.Builder()
                .baseUrl(URLPOSTNOTIFICATION)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

}
