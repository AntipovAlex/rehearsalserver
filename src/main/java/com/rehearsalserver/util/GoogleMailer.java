package com.rehearsalserver.util;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import java.util.Properties;

import static com.rehearsalserver.util.Constants.SENDER_ADRESS;

/**
 * Created by Antipov on 17.01.2017.
 */



public class GoogleMailer extends Thread {


    private Map<String, String> credentials;
    private String messageSubject;
    private String messageText;

    public GoogleMailer(Map<String, String> credentials, String messageSubject, String messageText) {
        this.credentials = credentials;
        this.messageSubject = messageSubject;
        this.messageText = messageText;
    }

    public void sendEmail(final Map<String, String> credintials, String reciever, String subject, String text){
        //properties settings
        Properties props = new Properties();
        props.put("mail.mime.charset", "UTF-8"); //cyrillic charset!
        props.put("mail.smtp.host", "in-v3.mailjet.com");
        props.put("mail.smtp.socketFactory.port", "587");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "587");

        //get session
        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(credintials.get("username"),credintials.get("password"));
                    }
                });
        //try to send email
        try {
            MimeMessage message = new MimeMessage(session);
            message.setContent(text, "text/html;charset=UTF-8");
            message.setFrom(new InternetAddress(SENDER_ADRESS));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(reciever));
            message.setSubject(subject, "UTF-8");
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void run() {
        sendEmail(credentials, "rigtor665@gmail.com", messageSubject, messageText);
    }
}
