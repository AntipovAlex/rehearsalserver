package com.rehearsalserver.util;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Antipov on 18.04.17.
 */
public class Constants {
    public static final String KEY = "My86gG7FBN5GbKhP";
    public static final String HEADER = "Authentication";
    public static final Long EXPIRATION = Long.valueOf(3600000); // one hour
    public static final String EMAIL_USERNAME = "70e63a79a565c4715d63641838887116";
    public static final String EMAIL_PASSWORD = "f29c1db7a8ac67cf55f9123300cbb0d4";

    public static final short FREE = 0;
    public static final short WAITING = 1;
    public static final short BOOKED = 2;

    /**
     * WTF is that? see there http://emailregex.com/
     */
    public static final String REG_EXP_EMAIL =
            "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-" +
                    "\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-" +
                    "\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" +
                    "\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]" +
                    "?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*" +
                    "[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\" +
                    "[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    public static final String REG_EXP_PHONE = "(3)80([0-9]{9}$)";
    public static final List<Integer> BOOKING_TYPES = Arrays.asList(0,1,2);
    public static final Integer PAGESIZE = 15;

    /**
     * VALIDATOR MESSAGES
     */
    public static final String LOGINERROR = "Логин не должен быть пустым либо состоять из пробелов";
    public static final String LOGINERRORSIZE = "Логин должен быть от 3 до 15 символов";
    public static final String PASSWORDERROR = "Пароль не должен быть пустым либо состоять из пробелов";
    public static final String PASSWORDERRORSIZE = "Пароль должен быть от 6 до 15 символов";
    public static final String SENDMESSAGECONTENTTYPE = "application/json";
    public static final String CRONEXPRESSION = "0 0 8 1/1 * ? *";
    public static final String URLPOSTNOTIFICATION = "https://fcm.googleapis.com";
    public static final String FCMAPIKEY =
            "Key=AAAAS1dZ7cQ:APA91bHtL9RkeVB6sNVWrr3Xw3rec-6OvxLnWJo0iVy73oZkmEsV4h1r2qhG8qJwfp2CPdGg_B_A1YEvfQXZZGU1WdkQKa_IHtjNTzU0dxoGPd77gb_QgRMBzmgvZt5-WXHMPJyv2KvR";

    public static final String SENDER_ADRESS = "contact@brutaltech.com.ua";

}
