package com.rehearsalserver.service;

import com.rehearsalserver.model.entity.BookingEntity;
import com.rehearsalserver.model.entity.UserEntity;
import com.rehearsalserver.util.HibernateUtil;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.annotation.Transient;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.rehearsalserver.util.Constants.BOOKED;
import static com.rehearsalserver.util.Constants.PAGESIZE;

/**
 * Created by Antipov on 04.06.17.
 */

@Service("bookingService")
@Transactional

public class BookingService {
    @PersistenceContext
    EntityManager em;

    @Transactional
    public List<BookingEntity> getByKey(String key, Integer page) {
        try (Session session = HibernateUtil.getSessionFactory()) {
            Calendar cal;
            Date date;
            // get end of the current day
            cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            cal.set(Calendar.MILLISECOND, 999);
            date = cal.getTime();

            Keys keys = Keys.valueOf(key.toUpperCase());
            session.beginTransaction();

            //get authenticated user
            UserEntity user = (UserEntity) SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal();

            List list = null;
            Integer firstRes = (PAGESIZE * page) - PAGESIZE;
            switch (keys) {
                case PAST:
                    list = session.createCriteria(BookingEntity.class)
                            .setFirstResult(firstRes)
                            .setMaxResults(PAGESIZE)
                            .add(Restrictions.eq("user", user))
                            .add(Restrictions.lt("time", new Date()))
                            .addOrder(Order.desc("time")).list();
                    break;
                case TODAY:
                    list = session.createCriteria(BookingEntity.class)
                            .setFirstResult(firstRes)
                            .setMaxResults(PAGESIZE)
                            .add(Restrictions.eq("user", user))
                            .add(Restrictions.gt("time", new Date()))
                            .add(Restrictions.lt("time", date))
                            .addOrder(Order.asc("time")).list();
                    break;

                case INFUTURE:
                    list = session.createCriteria(BookingEntity.class)
                            .setFirstResult(firstRes)
                            .setMaxResults(PAGESIZE)
                            .add(Restrictions.eq("user", user))
                            .add(Restrictions.gt("time", date))
                            .addOrder(Order.asc("time")).list();
                    break;
            }
            return list;
        }
    }


    @Transactional
    public List<BookingEntity> getBookingByDay(String year, String month, String day) throws ParseException {
        try  (Session session = HibernateUtil.getSessionFactory()) {
            session.beginTransaction();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
            //it value that means day start. E.g. 2017-04-03 00:00:00:000000
            Timestamp dayStart;


            dayStart = new Timestamp((dateFormat.parse(
                    year + "-" + month + "-" + day + " 00:00:00.0")).getTime());

            //it value that means day start. E.g. 2017-04-03 23:59:59:999999
            Timestamp dayEnd = null;

            dayEnd = new Timestamp((dateFormat.parse(
                    year + "-" + month + "-" + day + " 23:59:59.999")).getTime());

            return session.createCriteria(BookingEntity.class)
                    .add(Restrictions.gt("time", dayStart))
                    .add(Restrictions.lt("time", dayEnd)).list();
        }
    }


    @Transactional
    public Integer getNotification() {
        try (Session session = HibernateUtil.getSessionFactory()){
            Calendar cal;
            Date date;

            // get end of the current day
            cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, 23);
            cal.set(Calendar.MINUTE, 59);
            cal.set(Calendar.SECOND, 59);
            cal.set(Calendar.MILLISECOND, 999);
            date = cal.getTime();

            //get authenticated user
            UserEntity user = (UserEntity) SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal();
            int i = session.createCriteria(BookingEntity.class)
                    .add(Restrictions.eq("user", user))
                    .add(Restrictions.gt("time", new Date()))
                    .add(Restrictions.lt("time", date))
                    .addOrder(Order.asc("time")).list().size();
            HibernateUtil.shutdown();

            return i;
        }
    }

    @Transient
    @Transactional
    public void setBookingByDate(Integer year, Integer month, Integer day, String times,
                          String name, String telephone, String comment, Integer type) throws Throwable {
        try (Session session = HibernateUtil.getSessionFactory()) {
            //parsing time from string to timestamp
            List<Integer> time = new ArrayList<Integer>();
            List<Timestamp> timestamp = new ArrayList<Timestamp>();
            for (String s : times.split(",")) {
                time.add(Integer.valueOf(s));
            }
            for (Integer i : time) {
                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy/HH/mm/ss");
                try {
                    timestamp.add(
                            new Timestamp(
                                    dateFormat.parse(day + "/" + month + "/" + year + "/"
                                            + i + "/00/00").getTime()
                            )
                    );
                } catch (ParseException e) {
                    throw new Throwable("Ошибка при создании записи: ");
                    //return Response.fail(null, "Ошибка при создании записи: ".concat(e.getMessage()));
                }
            }
            //validating time for unique. If unique - throw exception
            validateTime(timestamp);
            //sending data to db
            for (Timestamp t : timestamp) {
                session.beginTransaction();
                //creating entity
                BookingEntity booking = new BookingEntity();
                UserEntity user;
                //check - if user is authed - set user_id
                if (!(SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal()).equals("anonymousUser")) {
                    String username = ((UserEntity) SecurityContextHolder.getContext()
                            .getAuthentication().getPrincipal()).getUsername();
                    user =
                            (UserEntity) (session.createCriteria(UserEntity.class)
                                    .add(Restrictions.eq("username", username)).list()).get(0);
                } else { //if not authed - set to anonymous
                    user =
                            (UserEntity) (session.createCriteria(UserEntity.class)
                                    .add(Restrictions.eq("username", "anonymous")).list()).get(0);
                }
                //filling entity fields from get-query
                booking.setTime(t);
                booking.setName(name);
                booking.setTelephone(telephone);
                booking.setComment(comment);
                booking.setType(type);
                booking.setUsers_id(user);
                booking.setBooked(false);
                //saving entity
                session.save(user);
                session.save(booking);
                //commit transaction
                session.getTransaction().commit();
            }
        }
    }
    private void validateTime(List<Timestamp> timestamp) throws Throwable {
        for (Timestamp t:timestamp){
            try (Session session = HibernateUtil.getSessionFactory()) {
                List bookings =
                        session.createCriteria(BookingEntity.class)
                                .add(Restrictions.eq("time", t))
                                .add(Restrictions.eq("booked", true)).list();
                if (bookings.size() != 0) {
                    throw new Throwable("Это время уже занято");
                }
            }
        }
    }

    @Transactional
    public List<BookingEntity> getBookingsForNotifications(){
        try (Session session = HibernateUtil.getSessionFactory()){
            // checking bookings
            java.util.Calendar cal;
            Date date;
            // get end of the current day
            cal = java.util.Calendar.getInstance();
            cal.set(java.util.Calendar.HOUR_OF_DAY, 23);
            cal.set(java.util.Calendar.MINUTE, 59);
            cal.set(java.util.Calendar.SECOND, 59);
            cal.set(java.util.Calendar.MILLISECOND, 999);
            date = cal.getTime();

            return session.createCriteria(BookingEntity.class)
                    // get today bookings
                    .add(Restrictions.gt("time", new Date()))
                    .add(Restrictions.lt("time", date))
                    .add(Restrictions.ne("user", session.createCriteria(UserEntity.class)
                            .add(Restrictions.eq("username", "anonymous")).uniqueResult() // only by registred
                    )).list();
        }
    }

    public enum Keys {
        PAST,
        TODAY,
        INFUTURE
    }
}
