package com.rehearsalserver.service;

import com.rehearsalserver.model.entity.UserRoleEntity;
import com.rehearsalserver.security.utils.JWTTokenUtil;
import com.rehearsalserver.util.HibernateUtil;
import com.rehearsalserver.model.entity.UserEntity;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import java.sql.Timestamp;

import static com.rehearsalserver.util.Constants.*;

/**
 * Created by antipov on 4/12/17.
 */

@Service("userService")
@Transactional
public class UserService implements UserDetailsService {
    @Override
    @Transactional
    public UserEntity loadUserByUsername(String s) throws UsernameNotFoundException {
        try (Session session = HibernateUtil.getSessionFactory()) {
            session.beginTransaction();
            UserEntity user = (UserEntity) (session.createCriteria(UserEntity.class).add(Restrictions.eq("username", s)).list()).get(0);
            return user;
        }
    }

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JWTTokenUtil jwtTokenUtil;

    @Transactional
    public UserEntity getToken( String username, String password, String deviceToken){
        try (Session session = HibernateUtil.getSessionFactory()) {
            // Perform the security
            final Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            username,
                            password
                    )
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);

            // Reload password post-security so we can generate token
            UserEntity user = loadUserByUsername(username);
            String token = jwtTokenUtil.generateToken(user);
            session.beginTransaction();
            user.setDeviceToken(deviceToken);
            session.update(user);
            session.getTransaction().commit();
            user.setToken(token);
            return user;
        }
    }

    @Transactional
    public void register(String username, String password, String first, String last,  String email, String phone) throws Exception {
        try (Session session = HibernateUtil.getSessionFactory()) {
            //creating session for adding new user
            session.beginTransaction();
            checkUnique(username, email, session);
            UserEntity newUser = new UserEntity();
            newUser.setUsername(username);
            //set hashed password
            newUser.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
            newUser.setEnabled(true);
            newUser.setEmail(email);
            newUser.setFirstName(first);
            newUser.setLastName(last);
            newUser.setPhone(phone);
            newUser.setPasswordResetAt(new Timestamp(System.currentTimeMillis()));
            session.save(newUser);

            UserRoleEntity roles = new UserRoleEntity();
            roles.setRole("ROLE_USER");
            roles.setUser(newUser);
            newUser.getUserRoles().add(roles);
            session.save(roles);
            session.getTransaction().commit();
        }
    }

    @Transactional
    public void updateDeviceToken(@AuthenticationPrincipal UserEntity principal, String token){
        try (Session session = HibernateUtil.getSessionFactory()) {
            session.beginTransaction();
            UserEntity user = (UserEntity) (session.createCriteria(UserEntity.class)
                    .add(Restrictions.eq("id", principal.getId()))).list().get(0);
            user.setDeviceToken(token);
            session.update(user);
            session.getTransaction().commit();
        }
    }

    private void checkUnique(String username, String email, Session session) throws Exception {
        // unfortunately, i can't get unique constraint name in exception, so i decided go some shitty code way ...
        if (!(session.createCriteria(UserEntity.class)
                .add(Restrictions.eq("username", username)).list().isEmpty())){
            throw new Exception("Пользователь с таким именем уже существует");
        }
        if (!(session.createCriteria(UserEntity.class)
                .add(Restrictions.eq("email", email)).list().isEmpty())){
            throw new Exception("Пользователь с такой почтой уже существует");
        }
    }

    private void checkEmail(String email, Session session) throws Exception{
        if (!(session.createCriteria(UserEntity.class)
                .add(Restrictions.eq("email", email)).list().isEmpty())){
            throw new Exception("Пользователь с такой почтой уже существует");
        }
    }

    @Transactional
    public void updateUser(@AuthenticationPrincipal UserEntity principal,
                           String first, String last, String email, String phone) throws Exception {
        try (Session session = HibernateUtil.getSessionFactory()) {
            session.beginTransaction();
            UserEntity user = (UserEntity) (session.createCriteria(UserEntity.class)
                    .add(Restrictions.eq("id", principal.getId()))).list().get(0);
            user.setFirstName(first);
            user.setLastName(last);
            user.setPhone(phone);
            if(!email.equals(user.getEmail())){
                checkEmail(email, session);
                user.setEmail(email);
            }
            session.update(user);
            session.getTransaction().commit();
        }
    }
}
